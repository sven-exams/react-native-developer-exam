import * as React from 'react';
import {
  ActivityIndicator,
  Button,
  StatusBar,
  StyleSheet,
  View,
  Text,
  AsyncStorage,
  TouchableOpacity
} from 'react-native';
import PhotosComponent from '../../components/PhotosComponent';
// import { TouchableOpacity } from 'react-native-gesture-handler';


class HomeScreen extends React.Component {
    static navigationOptions = {
        title: 'Home',
    };

    render() {
        return (
        <View style={styles.container}>
            <TouchableOpacity
                    onPress={this.AddItemScreen}
                >
                <View style={{width: '100%',zIndex:999,backgroundColor: '#2416EB'}}>
                    <Text style={{paddingTop: 10, paddingBottom: 10, fontSize: 24, color: '#ffffff', textAlign: 'center'}}>+ Add New Item</Text>
                </View>
            </TouchableOpacity>
            <View>
                <PhotosComponent 
                    _onClickDetailScreen={this._onClickDetailScreen}
                />
            </View>
        </View>
        );
    }

    // _detailScreen = () => {
    //     this.props.navigation.navigate('DetailScreen');
    // }

    AddItemScreen = () => {
        this.props.navigation.navigate('AddItemScreen');
    };

    _onClickDetailScreen = (item) => {
        this.props.navigation.navigate('DetailScreen', { item });
    };
}
  
const styles = StyleSheet.create({
container: {
    zIndex: 5,
    flex: 1,
    position: 'relative',
    paddingBottom: 60,
    backgroundColor: '#eeeeee'
},
});

export default HomeScreen;