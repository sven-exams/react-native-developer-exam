import * as React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    FlatList,
    Image,
    TouchableWithoutFeedback,
    Button,
} from 'react-native';

class DetailScreen extends React.Component {
    static navigationOptions = {
        title: 'DetailScreen',
    };

    render() {

        const { item } = this.props.navigation.state.params

        return (
        <View style={styles.container}>
            <Image
                style={{width: '100%', height: 300}}
                source={{uri: item.url}}
            />
            <View>
                <Text style={{textAlign: 'left', marginTop: 15, fontWeight: '600'}}>{item.title}</Text>
                <Text style={{marginTop: 15}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</Text>
            </View>
        </View>
        );
    }

    _showMoreApp = () => {
        this.props.navigation.navigate('Other');
    };
 
}
  
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 10,
        marginRight: 10,
        padding: 15,
        borderRadius: 12,
        shadowColor: '#000',
        shadowOffset: { width: 2, height: 2 },
        shadowOpacity: 0.4,
        shadowRadius: 2,
        elevation: 1,
    },
});

export default DetailScreen;