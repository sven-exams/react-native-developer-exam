import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  FlatList,
  Image
} from 'react-native';


const ModalComponent = ({children}) => {

    return(
        <View style={styles.container}>
            <View style={styles.innerContainer}>
                {children}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: "center",
        position: 'absolute',
        textAlign: 'center',
        top: 0, 
        bottom: 0, 
        left: 0, 
        right: 0,
        width: '100%',
        height: 700,
        backgroundColor: 'rgba(0,0,0,0.8) ',
        zIndex: 999,
        marginLeft: 'auto',
        marginRight: 'auto'
    },
    innerContainer: {
        backgroundColor: '#ffffff',
        paddingTop: 30,
        paddingLeft: 30,
        paddingRight: 30,
        paddingBottom: 30,
    }
})

export default ModalComponent;