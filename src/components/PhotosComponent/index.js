import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  FlatList,
  TouchableWithoutFeedback,
  Button,
  Alert
} from 'react-native';
import axios from 'axios';
import Item from '../ItemComponent/';
import ModalComponent from '../ModalComponent/ModalComponent';
import { TouchableHighlight } from 'react-native-gesture-handler';

class App extends Component {

    state= {
        photos: []
    }

    async componentDidMount() {
        await this.getPhotos();
    }

     getPhotos = async () => {

        try {
            const response = await axios.get('https://jsonplaceholder.typicode.com/photos?_start=1&_limit=10');
            this.setState({
                photos: response.data
            })
        } catch (error) {
            console.error(error);
        }

    }

    componentWillUnmount = () => {
        this.setState({
            photos: null
        })
    }

    deleteItem = async (id) => {
        Alert.alert(
            'Alert!!!',
            'Are you sure you want to delete',
            [
              {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              {text: 'OK', onPress: () => {

                try {

                    this.setState(prevState => ({
                        photos: prevState.photos.filter(photo => {
                        return photo.id !== id;
                        })
                    }))
        
                } catch (error) {
                    console.error(error);
                }

              }},
            ],
            {cancelable: false},
          );
        
     
    }

    renderItem = ({item}) => {
        return (
            <Item 
                item={item}
                deleteItem={this.deleteItem}
                _onClickDetailScreen={this.props._onClickDetailScreen}
            />
        );
    }

    render() {
        return(
            <SafeAreaView>
            <FlatList
                data={this.state.photos && this.state.photos}
                renderItem={this.renderItem}
                keyExtractor={(item, index) => index.toString()}
                />
            </SafeAreaView>
        )
    }
}



const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: 'red'
  }
})

export default App;
